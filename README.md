Twitter Data Mining
===================

# Info
This project aims at building a data miner for twitter feeds.
It aims to collect all the data regarding a tweet which confirms
to the query provided by the user.

# Dependencies
This project was build using following **System** dependencies.
* `Python 3.7.1`.
* `pip 19.0.3`.
* `setuptools 40.8.0`.

# Contact
* [onlinejudge95](mailto:onlinejudge95@gmail.com)
* [Issue Tracker](https://gitlab.com/oj95_data_mining/twitter_data_mining/boards/997732)
